const jwt = require('jsonwebtoken')

module.exports = {


  friendlyName: 'JSON Web Token helper',


  inputs: {

    user: {
      type: {},
      required: true
    }

  },


  exits: {

    success: {
      outputFriendlyName: 'JWT Token',
      outputType: {}
    }

  },


  fn: async function(inputs) {

    return jwt.sign({ id: inputs.user.emailAddress }, sails.config.jwt, {
      expiresIn: 86400 // expires in 24 hours
    });

  }

};
