const jwt = require('jsonwebtoken')

module.exports = {


  friendlyName: 'JSON Web Token helper',


  inputs: {

    token: {
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      outputFriendlyName: 'JWT verified successfully',
      outputType: 'string'
    },

    unauthorized: {
      outputFriendlyName: 'JWT verification failed'
    },

    notFound: {
      outputFriendlyName: 'JWT verified successfully, but user not found'
    }

  },


  fn: async function(inputs, exits) {

    return jwt.verify(inputs.token, sails.config.jwt, (err, authorizedData) => {
      if (err) {
        throw 'unauthorized';
      }

      // TODO if token is almost expired, issue new one (or not?)  

      return exits.success(authorizedData.id)
    });

  }

};
