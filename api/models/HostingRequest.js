/**
 * HostingRequest.js
 */

module.exports = {

  attributes: {

    // primitives

    message: {
      type: 'text',
    },

    status: {
      type: 'string',
      isIn: ['pending', 'accepted', 'expired'],
      required: true
    },

    city: {
        type: 'string',
        required: true
    },

    country: {
        type: 'string',
        required: true
    },
    
    dateFrom: {
      type: 'number',
      required: true
    },
    
    dateTo: {
      type: 'number',
      required: true
    },

    // associations

    guests: {
      collection: 'User'
    },

    host: {
      model: 'User'
    }

  },

  afterCreate: async function (model, proceed) {

    const usersToNotify = await User.find({
      where: {city: model.city},
      select: ['emailAddress']
    }).populate('pushSubscriptions')

    const roomNames = _.map(usersToNotify, 'emailAddress')

    sails.sockets.broadcast(roomNames, 'newHostingRequest', model);

    let pushSubscriptions = _.map(usersToNotify, 'pushSubscriptions')

    console.log(pushSubscriptions)





    // let roomName = model.city.toLowerCase()



    // const vapidKeys = {
    //   publicKey:
    //     'BLbMXrE_UDUYnPwsdUVoxPYklVGDhlhWvJA7C9PZaWLwIf8rbagY1u8m_l4Z9xmQZp5BBWqd3I3pU-jiza4Ah84',
    //   privateKey: '5djz_3SILjj7YQ1t2be6G81hpXHWCYoounG4uWpvkVo',
    // }

    // const webpush = require('web-push')

    // webpush.setVapidDetails(
    //   'mailto:jankulma@gmail.com',
    //   vapidKeys.publicKey,
    //   vapidKeys.privateKey
    // )

    // let sub = await Sub.find().limit(1)
    // sub = sub[0]

    // console.log(sub)

    // let lolsub = {
    //   endpoint: sub.endpoint,
    //   expirationTime: null,
    //   keys: {
    //     p256dh: sub.p256dh,
    //     auth: sub.auth
    //   }
    // };


    // console.log(lolsub)
    // console.log('||||||||||||||||||||||||||||||||||')

    // webpush.sendNotification(lolsub, model.message)

    return proceed()


  }

};
