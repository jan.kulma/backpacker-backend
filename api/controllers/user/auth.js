module.exports = {


  friendlyName: 'Authenticate user via JWT',

  inputs: {

    jwt: {
      type: 'string',
      required: true
    },
  },


  exits: {
    unauthorized: {
      statusCode: 403
    }
  },


  fn: async function (inputs, exits) {

    let emailAddress = await sails.helpers.jwt.verify(inputs.jwt)

    if (!emailAddress) { throw 'unauthorized' }

    const user = await User.findOne({ emailAddress })

    const token = await sails.helpers.jwt.sign(user)

    return exits.success({
      user,
      jwt: token
    })

  }

};
