module.exports = {


  friendlyName: 'Login',


  inputs: {

    emailAddress: {
      type: 'string',
    },

    password: {
      type: 'string',
    },

  },


  exits: {

    badCombo: {
      statusCode: 401
    }

  },


  fn: async function (inputs, exits) {

    // if JWT was stored
    let userRecord = this.req.me

    if (!userRecord) {
      userRecord = await User.findOne({
        emailAddress: inputs.emailAddress.toLowerCase(),
      });

      if(!userRecord) {
        throw 'badCombo';
      }

      await sails.helpers.passwords
        .checkPassword(inputs.password, userRecord.password)
        .intercept('incorrect', 'badCombo');
    }

    const token = await sails.helpers.jwt.sign(userRecord)

    return exits.success({
      user: userRecord,
      jwt: token
    })

  }

};
