/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  '*': 'is-logged-in',

  // // Bypass the `is-logged-in` policy for:
  'user/auth': true,
  'user/login': true,
  'user/register': true,
  // 'account/logout': true,
  // 'view-homepage-or-redirect': true,
  // 'deliver-contact-form-message': true,


  // for convinience only
  'hostingrequest/create': true,
  'test': true

};
